from datetime import date

from django.db import models

# Create your models here.
from django.db import models
from django.db.models import fields

from . import utility_scripts

# class Bout(models.Model):
#     date = models.DateField(primary_key=True, verbose_name="date", unique=True, null=False, default='1920-01-01')
#     #rowid = models.IntegerField(primary_key=True)
#     thanks = models.CharField(max_length=200, blank=True, null=True)
#     test = 1
#
#     #age = utility_scripts.date_age(date)
#
#     def __str__(self):
#         return  self.date
#
#     class Meta:
#         managed = True
#         db_table = 'bout'
#         ordering = ['-date']
#
#     def age(self):
#         return ((date.today() - self.date).days)
#
#     def pretty_date(self):
#         return utility_scripts.pretty_date(self.date, '%A, {S} %B %Y')
#
#
# class Chump(models.Model):
#     rowid = models.IntegerField(primary_key=True)
#     date = models.ForeignKey(Bout, models.DO_NOTHING, related_name='bouts', to_field='date')
#     name = models.CharField(max_length=200, blank=True, null=True)
#     image = models.CharField(max_length=200, blank=True, null=False)
#     url = models.CharField(max_length=200, blank=True, null=True)
#
#     def __str__(self):
#         return self.date + "/" + self.name
#
#     class Meta:
#         managed = True
#         db_table = 'chump'
#
#
# class test(models.Model):
#     test = models.IntegerField(primary_key=True)
#
#     class Meta:
#         managed = True
#         db_table = 'test'

class Bout(models.Model):
    #date = models.DateField(primary_key=True)
    date = models.CharField(max_length=10,primary_key=True)
    thanks = models.CharField(max_length=200, blank=True, null=True)
    #chumps = models.ManyToManyField(Chump)

    class Meta:
        managed = False
        db_table = 'bout'
        default_related_name = 'bouts'
        ordering = ['-date']

    def __str__(self):
        return 'Bout: ' + utility_scripts.pretty_date(self.date, '%Y-%m-%d')

    def age(self):
        return ((date.today() - self.date).days)

    def pretty_date_full(self):
        return utility_scripts.pretty_date(self.date, '%A, {S} %B %Y')

    def pretty_day(self):
        return utility_scripts.pretty_date(self.date, '%A')

    def pretty_date(self):
        return utility_scripts.pretty_date(self.date, '{S} %B %Y')

    def int_date(self):
        return utility_scripts.datetime_to_integer(self.date)


class Chump(models.Model):
    rowid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    image = models.CharField(max_length=200)
    url = models.CharField(max_length=200, blank=True, null=True)
    date = models.ForeignKey(Bout, models.DO_NOTHING, to_field='date', related_name="chumps")

    class Meta:
        managed = False
        db_table = 'chump'
        default_related_name = 'chumps'

    def __str__(self):
        return 'Chump: {}, {}'.format(utility_scripts.pretty_date(self.date.date, '%Y-%m-%d'), self.name)

    def get_absolute_url(self):
        return f"/history/"