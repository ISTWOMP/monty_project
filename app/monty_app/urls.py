from . import views


from django.urls import path
from django.conf.urls import include, url

from . import views
from .feeds import ChumpFeed

urlpatterns = [
    path('', views.index, name='index'),
    path('contact/', views.contact, name='contact'),
    path('history/', views.history, name='history'),
    path('history_dev/', views.history_dev, name='history_dev'),
    path('feed/', ChumpFeed(), name='post_feed'),
    path('api/', views.api, name='api'),
]

