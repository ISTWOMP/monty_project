
$.tablesorter.addParser({
  // set a unique id
  id: 'date',
  is: function(s, table, cell, $cell) {
    // return false so this parser is not auto detected
    return false;
  },
  format: function(s, table, cell, cellIndex) {
    // format your data for normalization
    return $(cell).find(".int_date").text();
  },
  // set type, either numeric or text
  type: 'numeric'


});

$(function() {
    var options = {
        widthFixed : true,
        showProcessing: true,
        cssAsc: 'up',
        cssDesc: 'down',
        sortList: [[0,1]],
    };

  $("table").tablesorter(options);

});
