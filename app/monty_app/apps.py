from django.apps import AppConfig


class MontyAppConfig(AppConfig):
    name = 'monty_app'
