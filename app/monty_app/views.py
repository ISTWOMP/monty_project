import re
from datetime import datetime
import requests

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render
from django.db.models import prefetch_related_objects

from monty_project import settings
from .models import *
from .utility_scripts import date_age, days_between
from statistics import median

import json
# Create your views here.
def index(request):
    two_newest_bouts = Bout.objects.order_by("-date")[0:2]
    previous_streak = (two_newest_bouts[0].date - two_newest_bouts[1].date)
    newest_chumps = two_newest_bouts[0].chumps.all()

    bouts = Bout.objects.all().prefetch_related("chumps").all()
    bouts = utility_scripts.add_dates_as_winner_to_bouts(bouts)

    from statistics import median, quantiles
    array = []
    for bout in bouts:
        if isinstance(bout.streak, int):
            array.append(bout.streak)
    quartiles = [int(x) for x in quantiles(array, n=4)]

    lower_fence = max(0, quartiles[0] - (1.5 * (quartiles[2] - quartiles[1])))
    upper_fence = quartiles[2] + (1.5 * (quartiles[2] - quartiles[1]))
    # print(median(array))
    # print(lower_fence)
    # print(quartiles)
    # print(upper_fence)
    # print(bouts[0].streak)

    prediction_text = ""

    if bouts[0].streak < quartiles[0]:
        prediction_text = "Not expecting a 'bout'"
    elif quartiles[1] < bouts[0].streak < quartiles[2]:
        prediction_text = "Due for a 'bout'"
    elif quartiles[2] < bouts[0].streak < upper_fence:
        prediction_text = "Overdue for a 'bout'"
    elif bouts[0].streak < upper_fence:
        prediction_text = "Waaay overdue for a 'bout'"

    # print(prediction_text)



    return render(request, "index.djt", {
        'newest_bout': two_newest_bouts[0],
        'previous_streak': previous_streak.days,
        'chumps': newest_chumps,
        'quartiles': quartiles,
        'prediction_text': prediction_text,
        'max': max(array),
    })


def history(request):
    # Works in following the ForeignKey
    # queryset = Chump.objects.select_related('date').all()
    # for chumps in queryset:
    #    chumps.date.thanks

    # Reverse forign key. Ususally would be "Cump_set", but Meta.default_related_name overrides this
    # In the template, call bout.chumps.all
    bouts = Bout.objects.all().prefetch_related("chumps").all()
    bouts = utility_scripts.add_dates_as_winner_to_bouts(bouts)

    return render(request, "history.djt", {'bouts': bouts})

def history_dev(request):
    # Works in following the ForeignKey
    # queryset = Chump.objects.select_related('date').all()
    # for chumps in queryset:
    #    chumps.date.thanks

    # Reverse forign key. Ususally would be "Cump_set", but Meta.default_related_name overrides this
    # In the template, call bout.chumps.all
    bouts = Bout.objects.all().prefetch_related("chumps").all().order_by('-date')
    bouts = utility_scripts.add_dates_as_winner_to_bouts(bouts)

    return render(request, "history_dev.djt", {'bouts': bouts})

def contact(request):
    post_done = False
    error_occured = False
    form_message, name, email, return_message = "", "", "", ""

    if request.method == "POST":
        ''' Begin reCAPTCHA validation '''
        #recaptcha_response = request.POST.get('g-recaptcha-response')
        #data = {
        #    'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        #    'response': recaptcha_response
        #}
        #r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        #result = r.json()
		
        recaptcha_response = request.POST.get('g-recaptcha-response')
        data = {
            'secret': settings.HCAPTCHA_SECRET,
            'response': recaptcha_response
        }
        r = requests.post('https://hcaptcha.com/siteverify', data=data)
        result = r.json()
		
        ''' End reCAPTCHA validation '''

        if result['success']:
            try:
                utility_scripts.send_email(
                    {
                        "name": request.POST['name'],
                        "email": request.POST['email'],
                        "message": request.POST['message'],
                    }
                )

                return_message = "Thanks for your message!"
                post_done = True
            except:
                return_message = "Issue sending mail. Please email thomas.r.waller@gmail.com to say that Tom's screwed up the mail again. Thanks! :D"
                error_occured = True


        else:
            return_message = 'Invalid reCAPTCHA. Please try again.'
            error_occured = True

        name, email, form_message = request.POST['name'], request.POST['email'], request.POST['message']

    return render(request, "contact.djt", {"post_done": post_done,
                                           "return_message": return_message,
                                           "name": name,
                                           "email": email,
                                           "form_message": form_message,
                                           "error_occured": error_occured})

def api(request):
    after = request.GET.get('after', '')

    if after == "all":
        from django.core import serializers
        from django.forms.models import model_to_dict
        bouts = Bout.objects.all().prefetch_related("chumps").all()
        bouts = utility_scripts.add_dates_as_winner_to_bouts(bouts)
        # serialized_obj = serializers.serialize('json', [bouts, ])
        result = json.dumps(utility_scripts.bouts_to_json(bouts))

    elif re.match(r'\d{4}-\d{2}-\d{2}', after) is not None:
        date = datetime.strptime(after, "%Y-%m-%d")
        bouts = Bout.objects.filter(date__gt=date).all().prefetch_related("chumps").all()
        result = json.dumps(utility_scripts.bouts_to_json(bouts))
    else:
        # send help
        result = """<pre>
Monty API

OPTIONS
  after		Expected values:
		all 		- Will return all bouts
		YYYY-MM-DD	- Will return bouts after date provided

EXAMPLES
	https://howmanydayssincemontaguestreetbridgehasbeenhit.com/api?after=2020-01-01
	https://howmanydayssincemontaguestreetbridgehasbeenhit.com/api?after=all"
        </pre>"""

    return HttpResponse(result)
