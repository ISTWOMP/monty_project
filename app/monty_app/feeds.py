from django.contrib.syndication.views import Feed
from django.template.defaultfilters import truncatewords
from .models import Chump

class ChumpFeed(Feed):
    title = 'https://howmanydayssincemontaguestreetbridgehasbeenhit.com/Feeds'
    link = '/'
    description = 'The latest Chumps!'

    def items(self):
        return Chump.objects.all()

    def item_title(self, item):
        return item.name

    def item_description(self, item):
        return item.name
        
    def item_link(self, item):
        return item.url
