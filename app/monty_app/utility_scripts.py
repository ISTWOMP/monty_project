import datetime

def normalize_to_date(date):
    if type(date) is datetime.date:
        return date
    elif type(date) is str:
        return datetime.date.strptime(date, "%Y-%m-%d")
    elif type(date) is datetime:
        return date.date()
    elif type(date) is not date:
        raise ValueError("Expected object of either str, datetime or date, got {}".format(type(date)))

def normalize_to_datetime(date):
    if type(date) is datetime.datetime:
        return date
    elif type(date) is str:
        return datetime.datetime.strptime(date, "%Y-%m-%d")
    elif type(date) is datetime.date:
        return datetime.datetime.combine(date, datetime.datetime.min.time())
    elif type(date) is not date:
        raise ValueError("Expected object of either str, datetime or date, got {}".format(type(date)))


def days_between(d1, d2):
    return abs((normalize_to_datetime(d1) - normalize_to_datetime(d2)).days)

def date_age(d1):
    return days_between(d1, datetime.datetime.now().strftime("%Y-%m-%d") )

def suffix(d):
    return 'th' if 11<=d<=13 else {1:'st',2:'nd',3:'rd'}.get(d%10, 'th')

def pretty_date(t, format):
    return t.strftime(format).replace('{S}', str(t.day) + suffix(t.day))

def datetime_to_integer(dt_time):
    return 10000*dt_time.year + 100*dt_time.month + dt_time.day

def add_dates_as_winner_to_bouts(bouts):
    for idx in range(0, len(bouts)):
        if idx == 0:
            bouts[idx].streak = date_age(bouts[idx].date)
        elif idx == len(bouts) - 1:
            bouts[idx].streak = "?"
        else:
            bouts[idx].streak = days_between(bouts[idx].date, bouts[idx - 1].date)
        bouts[idx].idx = idx
    return bouts

def bouts_to_json(bouts):
    bouts = add_dates_as_winner_to_bouts(bouts)
    dict_result = []


    for bout in bouts:
        single_bout = {
            "date": datetime.date.strftime(bout.date, "%Y-%m-%d"),
            "thanks": bout.thanks,
            "streak": bout.streak,
            "chumps": []
        }

        for chump in bout.chumps.all():
            new_chump = {
                "name": chump.name,
                "image": chump.image,
                "url": chump.url
            }
            single_bout["chumps"].append(new_chump)
        dict_result.append(single_bout)
    return dict_result

def send_email(contents):
    # Import smtplib for the actual sending function
    import smtplib

    # Import the email modules we'll need
    from email.message import EmailMessage

    # Open the plain text file whose name is in textfile for reading.
    msg = EmailMessage()

    message = "A contact from Montague Bridge Website was just submitted:\n\n"
    for key in contents:
        message += "{}:{}\n".format(key, contents[key])

    msg.set_content(message)

    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = f'Montague Street Bridge contact form submitted!'
    msg['From'] = "thomas.r.waller@gmail.com"
    msg['To'] = "thomas.r.waller@gmail.com"

    # Send the message via our own SMTP server.
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.starttls()
    s.login("thomas.r.waller@gmail.com", "yzldukgcygddntbt")
    s.send_message(msg)
    s.quit()

if __name__ == "__main__":
    date_to_test = "2019-01-01"
    date_to_test_between = "2019-01-10"

    print("Testing days_between()")
    print("Testing diff between {} and {}".format(date_to_test, date_to_test_between))
    print("The difference is: {} days.".format(days_between(date_to_test, date_to_test_between)))
    print("")
    print("Testing date_age()")
    print("Testing date age of {}".format(date_to_test))
    print("The age is: {} days.".format(date_age(date_to_test)))
