#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}/../"

export GIT_COMMIT="$(git ls-remote https://ISTWOMP@bitbucket.org/ISTWOMP/monty_project.git HEAD | cut -f 1)"
#docker build --tag myimage:git-"$GIT_COMMIT" --build-arg GIT_REF="$GIT_COMMIT" .

docker-compose down -v
docker-comecvho pose -f docker-compose.prod.yml up --force-recreate -d --build